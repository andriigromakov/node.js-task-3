const {
  createTruck,
  asignTruckById,
  getTrucksByUserId,
  getTruckByIds,
  updateTruckTypeByIds,
  deleteTruckById,
} = require('../services/trucksService');

const getTrucks = async (req, res) => {
  res.status(200).json({
    trucks: await getTrucksByUserId(req.user._id),
  });
};
const getTruck = async (req, res) => {
  res.status(200).json({
    truck: await getTruckByIds({
      userId: req.user._id,
      truckId: req.params.id,
    }),
  });
};

const addTruck = async (req, res) => {
  await(createTruck({
    type: req.body.type,
    userId: req.user._id,
  }));
  res.status(200).json({
    message: 'Truck created successfully',
  });
};

const assignTruck = async (req, res) => {
  await asignTruckById({
    truckId: req.params.id,
    userId: req.user._id,
  });
  res.status(200).json({
    message: 'Truck assigned successfully',
  });
};

const updateTruck = async (req, res) => {
  await updateTruckTypeByIds({
    userId: req.user._id,
    truckId: req.params.id,
    type: req.body.type,
  });
  res.status(200).json({
    message: 'Truck details changed successfully',
  });
};
const deleteTruck = async (req, res) => {
  await deleteTruckById({
    userId: req.user._id,
    truckId: req.params.id,
  });
  res.status(200).json({
    message: 'Truck deleted successfully',
  });
};

module.exports = {
  addTruck,
  assignTruck,
  getTrucks,
  getTruck,
  updateTruck,
  deleteTruck,
};
