const {
  createLoad,
  updateLoadByIds,
  deleteLoadById,
  postLoadById,
  getLoadsByDriverId,
  getLoadsByShipperId,
  getLoadByDriverId,
  getLoadByShipperId,
  getActiveLoadByDriverId,
  getShippingInfoByIds,
  iterateLoadStateByDriverId,
} = require('../services/loadsService');
const {userStatusInfo: {DRIVER}} = require('../config/main');

const addLoad = async (req, res) => {
  await createLoad({
    userId: req.user._id,
    name: req.body.name,
    payload: req.body.payload,
    pickupAddress: req.body.pickup_address,
    deliveryAddress: req.body.delivery_address,
    dimensions: req.body.dimensions,
  });
  res.status(200).json({
    message: 'Load created successfully',
  });
};


const updateLoad = async (req, res) => {
  await updateLoadByIds({
    loadId: req.params.id,
    userId: req.user._id,
    loadData: req.body,
  });
  res.status(200).json({
    message: 'Load details changed successfully',
  });
};

const deleteLoad = async (req, res) => {
  await deleteLoadById({
    loadId: req.params.id,
    userId: req.user._id,
  });
  res.status(200).json({
    message: 'Load deleted successfully',
  });
};

const postLoad = async (req, res) => {
  const driverFound = await postLoadById({
    loadId: req.params.id,
    userId: req.user._id,
  });
  res.status(200).json({
    message: 'Load posted successfully',
    driver_found: driverFound,
  });
};

const getUserLoads = async (req, res) => {
  const isDriver = req.user.role === DRIVER;
  const options = {
    limit: req.query.limit,
    offset: req.query.offset,
    status: req.query.status,
  };
  let loads;
  if (isDriver) {
    loads = await getLoadsByDriverId({
      driverId: req.user._id,
      ...options,
    });
  } else {
    loads = await getLoadsByShipperId({
      shipperId: req.user._id,
      ...options,
    });
  }

  res.status(200).json({
    loads: loads,
  });
};

const getUserLoad = async (req, res) => {
  const isDriver = req.user.role === DRIVER;
  let load;
  if (isDriver) {
    load = await getLoadByDriverId({
      driverId: req.user._id,
      loadId: req.params.id,
    });
  } else {
    load = await getLoadByShipperId({
      shipperId: req.user._id,
      loadId: req.params.id,
    });
  }

  res.status(200).json({
    load: load,
  });
};

const getActiveLoad = async (req, res) => {
  const load = await getActiveLoadByDriverId(req.user._id);
  res.status(200).json({
    load: load,
  });
};

const getShippingInfo = async (req, res) => {
  const info = await getShippingInfoByIds({
    loadId: req.params.id,
    userId: req.user._id,
  });
  res.status(200).json(info);
};

const iterateToNextLoadState = async (req, res) => {
  const message = await iterateLoadStateByDriverId(req.user._id);
  res.status(200).json({
    message,
  });
};

module.exports = {
  addLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getUserLoads,
  getUserLoad,
  getActiveLoad,
  getShippingInfo,
  iterateToNextLoadState,
};
