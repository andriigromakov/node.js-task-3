const {
  createUser,
  logInUser,
  updateAndSendNewPassword,
} = require('../services/authService');

const fullURL = require('../utils/fullURL');

const login = async (req, res) => {
  const {email, password} = req.body;
  const token = await logInUser({email, password});
  res.status(200).json(token);
};

const register = async (req, res, next) => {
  const {email, password, role} = req.body;
  await createUser({email, password, role});
  res.status(200).json({
    message: 'Profile created successfully',
  });
};

const forgotPassword = async (req, res, next) => {
  const url = fullURL(req);
  await updateAndSendNewPassword({email: req.body.email, url: url});
  res.status(200).json({
    message: 'New password sent to your email address',
  });
};

module.exports = {
  login,
  register,
  forgotPassword,
};
