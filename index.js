require('dotenv').config();

const fs = require('fs');
const path = require('path');

const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');

const main = require('./routes/index');
const defaultErrorHandler = require('./utils/defaultErrorHandler');

const PORT = +process.env.PORT;
const DB_CONNECTION_STRING = process.env.DB_CONNECTION_STRING;

const loggerFile = path.join(__dirname, 'logs.log');

try {
  if (!fs.existsSync(loggerFile)) {
    fs.appendFileSync(loggerFile, '', 'utf8');
  }
} catch (error) {
  console.log(error);
}

const app = express();

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs.log'), {
  flags: 'a',
});

app.use(morgan('combined', {stream: accessLogStream}));

app.use(express.json());
app.use(cors());

app.use(main);
app.use(defaultErrorHandler);

const initialize = async () => {
  try {
    await mongoose.connect(DB_CONNECTION_STRING, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    app.listen(PORT || 8080);
  } catch (err) {
    console.error(`Failed to start server: ${err.message}`);
    return;
  }
  console.log('server is running at port ' + (PORT || 8080));
};

initialize();
