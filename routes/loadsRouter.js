const express = require('express');
const errorWrapper = require('../utils/errorWrapper');
const {
  addLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getUserLoads,
  getUserLoad,
  getActiveLoad,
  getShippingInfo,
  iterateToNextLoadState,
} = require('../controllers/loadsController');
const {
  addLoadValidator,
} = require('../middlewares/validation/index');
const shipperMiddleware = require('../middlewares/shipperMiddleware');
const driverMiddleware = require('../middlewares/driverMiddleware');
const router = new express.Router();

router.get('/', getUserLoads);
router.post('/', shipperMiddleware, addLoadValidator, errorWrapper(addLoad));
router.get('/active', driverMiddleware, errorWrapper(getActiveLoad));
router.patch(
    '/active/state', driverMiddleware, errorWrapper(iterateToNextLoadState),
);
router.get('/:id', errorWrapper(getUserLoad));
router.put('/:id', shipperMiddleware, errorWrapper(updateLoad));
router.delete('/:id', shipperMiddleware, errorWrapper(deleteLoad));
router.post('/:id/post', shipperMiddleware, errorWrapper(postLoad));
router.get(
    '/:id/shipping_info', shipperMiddleware, errorWrapper(getShippingInfo),
);

module.exports = router;
