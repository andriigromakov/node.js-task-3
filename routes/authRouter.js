const express = require('express');
const errorWrapper = require('../utils/errorWrapper');
const {
  register,
  login,
  forgotPassword,
} = require('../controllers/authController');
const {
  registerValidator,
  loginValidator,
  forgotPasswordValidator,
} = require('../middlewares/validation/index');

const router = new express.Router();

router.post('/register', registerValidator, errorWrapper(register));
router.post('/login', loginValidator, errorWrapper(login));
router.post(
    '/forgot_password', forgotPasswordValidator, errorWrapper(forgotPassword),
);

module.exports = router;
