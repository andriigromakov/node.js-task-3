const express = require('express');
const authMiddleware = require('../middlewares/authMiddleware');
const driverMiddleware = require('../middlewares/driverMiddleware');
const authRouter = require('./authRouter');
const usersRouter = require('./usersRouter');
const trucksRouter = require('./trucksRouter');
const loadsRouter = require('./loadsRouter');

const main = new express.Router();

main.use('/api/auth', authRouter);
main.use('/api/users/me', authMiddleware, usersRouter);
main.use('/api/trucks', authMiddleware, driverMiddleware, trucksRouter);
main.use('/api/loads', authMiddleware, loadsRouter);
module.exports = main;
