const mongoose = require('mongoose');
const {
  loadStatusList,
  loadStateList,
} = require('../config/main');

const Schema = mongoose.Schema;

const loadSchema = new Schema({
  created_by: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  status: {
    type: String,
    enum: loadStatusList,
    default: 'NEW',
    required: true,
  },
  state: {
    type: String,
    enum: loadStateList,
  },
  name: {
    type: String,
    required: true,
  },
  payload: Number,
  dimensions: {
    width: Number,
    length: Number,
    height: Number,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  logs: {
    type: [{
      message: {
        type: String,
        required: true,
      },
      time: {
        type: Date,
        default: Date.now(),
      },
    }],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },


});

module.exports = mongoose.model('Load', loadSchema);
