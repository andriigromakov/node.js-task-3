const mongoose = require('mongoose');
const {
  truckStatusList,
  truckTypeList,
} = require('../config/main');

const Schema = mongoose.Schema;

const userSchema = new Schema({
  created_by: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  type: {
    type: String,
    enum: truckTypeList,
    required: true,
  },
  status: {
    type: String,
    enum: truckStatusList,
    default: 'IS',
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  dimensions: {
    width: Number,
    length: Number,
    height: Number,
  },
  payload: Number,
});

module.exports = mongoose.model('Truck', userSchema);
