const {
  userStatusInfo: {SHIPPER},
} = require('../config/main');

const shipperMiddleware = (req, res, next) => {
  if (req.user.role !== SHIPPER) {
    const err = new Error('This action only available for shipper role');
    err.status = 400;
    throw err;
  }
  next();
};

module.exports = shipperMiddleware;
