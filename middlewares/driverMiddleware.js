const {
  userStatusInfo: {DRIVER},
} = require('../config/main');

const driverMiddleware = (req, res, next) => {
  if (req.user.role !== DRIVER) {
    const err = new Error('This action only available for driver role');
    err.status = 400;
    throw err;
  }
  next();
};

module.exports = driverMiddleware;
