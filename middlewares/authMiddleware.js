
const jwt = require('jsonwebtoken');

const SECRET = process.env.SECRET;

const User = require('../models/user');
const authMiddleware = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    if (!token) {
      throw new Error();
    }
    const userInfo = jwt.verify(token, SECRET);
    const userExists = await User.exists({_id: userInfo._id});
    if (!userExists) {
      return res.status(400).json({
        message: 'User not found',
      });
    }

    req.user = {
      _id: userInfo._id,
      role: userInfo.role,
      email: userInfo.email,
    };
    next();
  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: 'User not authorized',
    });
  }
};

module.exports = authMiddleware;
