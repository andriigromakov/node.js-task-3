﻿const Joi = require('joi');
const {
  userStatusList,
  truckTypeList,
} = require('../../config/main');

const dimensionsSchema = Joi.object({
  width: Joi.number().min(0).required(),
  length: Joi.number().min(0).required(),
  height: Joi.number().min(0).required(),
}).required();

const register = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  role: Joi.string().valid(...userStatusList).required(),
});

const login = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});
const forgotPassword = Joi.object({
  email: Joi.string().email().required(),
});

const updatePassword = Joi.object({
  oldPassword: Joi.string().required(),
  newPassword: Joi.string().required(),
});

const addTruck = Joi.object({
  type: Joi.string().valid(...truckTypeList).required(),
});

const updateTruck = Joi.object({
  type: Joi.string().valid(...truckTypeList).required(),
});

const postLoad = Joi.object({
  name: Joi.string().required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: dimensionsSchema,
});


module.exports = {
  register,
  login,
  forgotPassword,
  updatePassword,
  addTruck,
  updateTruck,
  postLoad,
};
