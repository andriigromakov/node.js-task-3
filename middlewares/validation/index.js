const {
  bodyValidator,
} = require('./createValidator');
const {
  register,
  login,
  forgotPassword,
  updatePassword,
  addTruck,
  updateTruck,
  postLoad,
} = require('./joiSchemas');

module.exports = {
  registerValidator: bodyValidator(register),
  loginValidator: bodyValidator(login),
  forgotPasswordValidator: bodyValidator(forgotPassword),
  updatePasswordValidator: bodyValidator(updatePassword),
  addTruckValidator: bodyValidator(addTruck),
  updateTruckValidator: bodyValidator(updateTruck),
  addLoadValidator: bodyValidator(postLoad),
};
