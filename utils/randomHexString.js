const crypto = require('crypto');

const randomHexString = (length) => crypto.randomBytes(length).toString('hex');

module.exports = randomHexString;
