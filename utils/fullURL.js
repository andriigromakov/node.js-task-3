const PORT = +process.env.PORT;
const fullURL = (req) => {
  return `${req.protocol}://${req.hostname}:${PORT || 8080}`;
};

module.exports = fullURL;
