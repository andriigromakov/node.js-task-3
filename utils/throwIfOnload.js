const {truckStatusInfo: {OL}} = require('../config/main');

const throwIfOnload = (status) => {
  if (OL === status) {
    const er = new Error('Action is not allowed while truck is on a load');
    er.status = 400;
    throw er;
  }
};

module.exports = throwIfOnload;
