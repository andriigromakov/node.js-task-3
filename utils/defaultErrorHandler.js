const defaultErrorHandler = (err, req, res, next) => {
  console.error(err.stack);
  console.log(err.status);
  console.log(err.message);
  if (err.status && err.message) {
    res.status(err.status).json({
      message: err.message,
    });
    return;
  }
  res.status( 500).json({
    message: 'Internal server error',
  });
};

module.exports = defaultErrorHandler;
