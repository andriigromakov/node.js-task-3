# node.js-task-3
UBER like service for freight trucks, in REST style. Created with Node.js, MongoDB and Mongoose.

## set up and launch
To run this project, after git pull, run these commands:
```
$ npm install
$ npm start
```

Now server is running at http://localhost:8080. Port may differ if you change it in .env file.

To run this project you need to install [Node.js](https://nodejs.org/en/).
