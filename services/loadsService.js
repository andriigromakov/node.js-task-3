const ObjectId = require('mongoose').Types.ObjectId;
const Load = require('../models/load');

const {
  getAssignedTruckIdByUserId,
  getAssignedTruckByUserId,
  findAvailableTruck,
  getAssignedTruckByTruckId,
} = require('../services/trucksService');
const {loadStateList,
  truckStatusInfo: {OL, IS},
  loadStatus: {
    NEW,
    POSTED,
    ASSIGNED,
    SHIPPED,
  }} = require('../config/main');

const transformLoads = (loads) => {
  return loads.map((load) => {
    load = load.toObject();
    load.logs = load.logs.map((log) => ({
      message: log.message,
      time: log.time,
    }));
    if (!load.assigned_to) {
      load.assigned_to = '';
    }
    if (!load.state) {
      load.state = '';
    }
    return load;
  });
};

const transformLoad = (load) => {
  load = load.toObject();
  load.logs = load.logs.map((log) => ({
    message: log.message,
    time: log.time,
  }));
  if (!load.assigned_to) {
    load.assigned_to = '';
  }
  if (!load.state) {
    load.state = '';
  }
  return load;
};

const createLoad = async ({
  userId,
  name,
  payload,
  pickupAddress,
  deliveryAddress,
  dimensions,
}) => {
  const load = await new Load({
    created_by: new ObjectId(userId),
    name,
    payload: +payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
  });

  await load.save();
};
const updateLoadByIds = async ({
  loadId,
  userId,
  loadData,
}) => {
  const load = await Load.findOne({_id: loadId, created_by: userId});
  if (!load) {
    const er = new Error('Load not found');
    er.status = 400;
    throw er;
  }

  if (load.status !== NEW) {
    const er = new Error('Only loads with status "NEW" can be updated');
    er.status = 400;
    throw er;
  }
  await Load.findOneAndUpdate(
      {_id: loadId, created_by: userId},
      {$set: loadData},
  );
};

const deleteLoadById = async ({
  loadId,
  userId,
}) => {
  const load = await Load.findOne({_id: loadId, created_by: userId});

  if (!load) {
    const er = new Error('Load not found');
    er.status = 400;
    throw er;
  }
  if (load.status !== NEW) {
    const er = new Error('Only loads with status "NEW" can be deleted');
    er.status = 400;
    throw er;
  }
  await Load.deleteOne({_id: loadId, created_by: userId});
};

const postLoadById = async ({
  loadId,
  userId,
}) => {
  const load = await Load.findOne({_id: loadId, created_by: userId});
  if (!load) {
    const er = new Error('Load not found');
    er.status = 400;
    throw er;
  }

  if (load.status !== NEW) {
    const er = new Error('Only loads with status "NEW" can be posted');
    er.status = 400;
    throw er;
  }

  await Load.findOneAndUpdate(
      {_id: loadId, created_by: userId},
      {$set: {
        status: POSTED,
      },
      $push: {
        logs: {
          message: `Load status changed to "${POSTED}"`,
          time: new Date(),
        },
      }},
  );
  const avTruck = await findAvailableTruck({
    payload: load.payload,
    width: load.dimensions.width,
    length: load.dimensions.length,
    height: load.dimensions.height,
  });
  if (!avTruck) {
    await load.updateOne(
        {$set: {
          status: NEW,
        },
        $push: {
          logs: {
            message: `Driver now found, status changed back to "${NEW}"`,
            time: new Date(),
          },
        }},
    );
    return false;
  }

  await load.updateOne(
      {$set: {
        status: ASSIGNED,
        assigned_to: new ObjectId(avTruck._id),
        state: loadStateList[0],
      },
      $push: {
        logs: {
          message: `status changed to ${ASSIGNED}, driver id: ${avTruck._id}`,
          time: new Date(),
        },
      }},
  );
  avTruck.status = OL;
  await avTruck.save();
  return true;
};

const getLoadsByDriverId = async ({
  driverId,
  limit = 10,
  offset = 0,
  status,
}) => {
  const query = {
    assigned_to: await getAssignedTruckIdByUserId(driverId),
  };

  if (status) {
    query.status = status;
  }

  const currentLimit = limit > 50 ? 50 : limit;

  const loads = await Load.find(query, '-__v')
      .skip(+offset)
      .limit(+currentLimit);
  return transformLoads(loads);
};

const getLoadsByShipperId = async ({
  shipperId,
  limit = 10,
  offset = 0,
  status,
}) => {
  const query = {
    created_by: shipperId,
  };

  if (status) {
    query.status = status;
  }

  const currentLimit = limit > 50 ? 50 : limit;

  const loads = await Load.find(query, '-__v')
      .skip(+offset)
      .limit(+currentLimit);

  return transformLoads(loads);
};


const getLoadByDriverId = async ({loadId, driverId}) => {
  const load = await Load.findOne({
    _id: loadId,
    assigned_to: await getAssignedTruckIdByUserId(driverId),
  },
  '-__v',
  );
  return transformLoad(load);
};

const getLoadByShipperId = async ({loadId, shipperId}) => {
  const load = await Load.findOne({
    _id: loadId,
    created_by: shipperId,
  },
  '-__v',
  );
  return transformLoad(load);
};

const getActiveLoadByDriverId = async (driverId) => {
  const load = await Load.findOne({
    assigned_to: await getAssignedTruckIdByUserId(driverId),
    status: ASSIGNED,
  },
  '-__v',
  );
  if (!load) {
    const err = new Error('Active load not found');
    err.status = 400;
    throw err;
  }
  return transformLoad(load);
};
const getShippingInfoByIds = async ({
  loadId,
  userId,
}) => {
  const load = await Load.findOne({
    _id: loadId,
    created_by: userId,
    status: ASSIGNED,
    assigned_to: {
      $ne: null,
    },
  },
  '-__v',
  );
  if (!load) {
    const err = new Error('Active load not found');
    err.status = 400;
    throw err;
  }
  const truck = await getAssignedTruckByTruckId(load.assigned_to);
  return ({
    load: transformLoad(load),
    truck,
  });
};

const iterateLoadStateByDriverId = async (driverId) => {
  const truck = await getAssignedTruckByUserId(driverId);
  const load = await Load.findOne({
    assigned_to: truck._id,
  });
  const stateIndex = loadStateList.findIndex((el) => el === load.state);
  if (stateIndex === -1) {
    throw new Error('Wrong load state is provided');
  }
  let newStateIndex;
  if (stateIndex === 3) {
    return (
      'Cannot update load state, state is already "Arrived to delivery"'
    );
  } else {
    newStateIndex = stateIndex + 1;
  }

  if (newStateIndex === 3) {
    await load.updateOne(
        {$set: {
          status: SHIPPED,
          state: loadStateList[newStateIndex],
        },
        $push: {
          logs: {
            message: `Load state changed to '${loadStateList[newStateIndex]}'`,
            time: new Date(),
          },
        }},
    );

    truck.status = IS;
    await truck.save();

    return `Load state changed to '${loadStateList[newStateIndex]}'`;
  }

  await load.updateOne(
      {$set: {
        state: loadStateList[newStateIndex],
      },
      $push: {
        logs: {
          message: `Load state changed to '${loadStateList[newStateIndex]}'`,
          time: new Date(),
        },
      }},
  );
  return `Load state changed to '${loadStateList[newStateIndex]}'`;
};

module.exports = {
  createLoad,
  updateLoadByIds,
  deleteLoadById,
  postLoadById,
  getLoadsByDriverId,
  getLoadsByShipperId,
  getLoadByDriverId,
  getLoadByShipperId,
  getActiveLoadByDriverId,
  getShippingInfoByIds,
  iterateLoadStateByDriverId,
};
