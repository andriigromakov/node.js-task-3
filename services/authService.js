const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

const randomHexString = require('../utils/randomHexString');
const User = require('../models/user');

const SECRET = process.env.SECRET;
const EMAIL = process.env.MAIL_CLIENT_LOGIN;
const PASSWORD = process.env.MAIL_CLIENT_PASSWORD;

const createUser = async ({email, password, role}) => {
  try {
    const userAlreadyExist = await User.exists({email});
    if (userAlreadyExist) {
      const err = new Error('Email is already taken');
      err.status = 400;
      throw err;
    }
    const hashedPassword = await bcrypt.hash(password, 12);
    const user = new User({email, role, password: hashedPassword});
    await user.save();
  } catch (error) {
    if (error.status && error.message) {
      throw error;
    }
    console.log(error);
    const err = new Error('Failed to create profile');
    err.status = 500;
    throw err;
  }
};

const logInUser = async ({email, password}) => {
  const user = await User.findOne({email});
  if (!user) {
    const err = new Error('Wrong email');
    err.status = 400;
    throw err;
  }
  const isEqual = await bcrypt.compare(password, user.password);
  if (!isEqual) {
    const err = new Error('Wrong password');
    err.status = 400;
    throw err;
  }
  const token = jwt.sign(
      {
        _id: user._id,
        role: user.role,
        email: user.email,
      },
      SECRET,
      {
        expiresIn: 86400,
      },
  );
  return {
    jwt_token: token,
  };
};

const updateAndSendNewPassword = async ({email, url}) => {
  const user = await User.findOne({email});

  if (!user) {
    const err = new Error('Wrong email');
    err.status = 400;
    throw err;
  }

  const tempPassword = randomHexString(32);

  const html = `
  <h1>Hello!</h1>
  <p>
    Your new password for <em>${url}</em> is: <strong>${tempPassword}</strong>
  </p>
  `;

  const mailTransporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: EMAIL,
      pass: PASSWORD,
    },
  });

  const mailDetails = {
    from: EMAIL,
    to: email,
    subject: 'Password change',
    html: html,
  };

  user.password = await bcrypt.hash(tempPassword, 12);
  await user.save();

  await mailTransporter.sendMail(mailDetails);
};

module.exports = {
  createUser,
  logInUser,
  updateAndSendNewPassword,
};
