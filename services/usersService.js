const User = require('../models/user');
const bcrypt = require('bcrypt');

const getUserById = async (_id) => {
  const user = await User.findOne({_id}, {
    __v: 0,
    password: 0,
  });
  if (!user) {
    const err = new Error('User not found');
    err.status = 500;
    throw err;
  }
  return {
    user,
  };
};

const updateUserPassword = async ({_id, oldPassword, newPassword}) => {
  const user = await User.findOne({_id});
  if (!user) {
    const err = new Error('User not found');
    err.status = 500;
    throw err;
  }
  // console.log(user);
  const isEqual = await bcrypt.compare(oldPassword, user.password);
  if (!isEqual) {
    const err = new Error('Wrong password');
    err.status = 400;
    throw err;
  }
  user.password = await bcrypt.hash(newPassword, 12);
  await user.save();
};

const deleteUserById = async (id) => {
  const result = await User.deleteOne({_id: id});
  if (result.deletedCount === 0) {
    const er = new Error('User not found');
    er.status = 400;
    throw er;
  }
};

module.exports = {
  getUserById,
  updateUserPassword,
  deleteUserById,
};
