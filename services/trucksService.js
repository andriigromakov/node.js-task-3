const ObjectId = require('mongoose').Types.ObjectId;
const Truck = require('../models/truck');
const {truckInfo, truckStatusInfo: {IS}} = require('../config/main');
const throwIfOnload = require('../utils/throwIfOnload');
const throwIfAssigned = require('../utils/throwIfAssigned');

const getTrucksByUserId = async (userId) => {
  const trucks = await Truck.find(
      {created_by: userId},
      {
        dimensions: 0,
        payload: 0,
        __v: 0,
      });
  return trucks.map(
      (truck) => {
        truck = truck.toObject();
        if (!truck.assigned_to) {
          truck.assigned_to = '';
        }
        return truck;
      },
  );
};

const getAssignedTruckIdByUserId = async (userId) => {
  const truck = await Truck.findOne({assigned_to: userId});
  if (!truck) {
    const er = new Error('Truck not found');
    er.status = 400;
    throw er;
  }
  return truck._id;
};

const getAssignedTruckByUserId = async (userId) => {
  const truck = await Truck.findOne({assigned_to: userId});
  if (!truck) {
    const er = new Error('Truck not found');
    er.status = 400;
    throw er;
  }
  return truck;
};

const getAssignedTruckByTruckId = async (truckId) => {
  const truck = await Truck.findOne({_id: truckId},
      {
        __v: 0,
        dimensions: 0,
        payload: 0,
      },
  );

  if (!truck) {
    const er = new Error('Truck not found');
    er.status = 400;
    throw er;
  }

  return truck;
};


const getTruckByIds = async ({userId, truckId}) => {
  let truck = await Truck.findOne( {created_by: userId, _id: truckId},
      {
        dimensions: 0,
        payload: 0,
        __v: 0,
      });
  if (!truck) {
    const er = new Error('Truck not found');
    er.status = 400;
    throw er;
  }
  truck = truck.toObject();
  if (!truck.assigned_to) {
    truck.assigned_to = '';
  }
  return truck;
};

const updateTruckTypeById = async ({truckId, type}) => {
  const truck = await Truck.findOne({_id: truckId});
  if (!truck) {
    const er = new Error('Truck not found');
    er.status = 400;
    throw er;
  }
  if (truck.type === type) {
    return;
  }
  const {payload: newPayload, dimensions: newDimensions} = truckInfo[type];
  truck.type = type;
  truck.payload = newPayload;
  truck.dimensions = newDimensions;
  await truck.save();
};

const updateTruckTypeByIds = async ({userId, truckId, type}) => {
  const truck = await Truck.findOne({created_by: userId, _id: truckId});
  if (!truck) {
    const er = new Error('Truck not found');
    er.status = 400;
    throw er;
  }
  throwIfOnload(truck.status);
  throwIfAssigned({
    userId,
    assignedTo: truck.assigned_to,
  });

  if (truck.type === type) {
    return;
  }

  const {payload: newPayload, dimensions: newDimensions} = truckInfo[type];
  truck.type = type;
  truck.payload = newPayload;
  truck.dimensions = newDimensions;
  await truck.save();
};

const deleteTruckById = async ({userId, truckId}) => {
  const truck = await Truck.findOne({
    _id: truckId,
    created_by: userId,
  });
  if (!truck) {
    const er = new Error('Truck not found');
    er.status = 400;
    throw er;
  }
  throwIfOnload(truck.status);
  throwIfAssigned({
    userId,
    assignedTo: truck.assigned_to,
  });

  const result = await Truck.deleteOne(
      {
        _id: truckId,
        created_by: userId,
      },
  );
  if (result.deletedCount === 0) {
    const er = new Error('Truck not found');
    er.status = 400;
    throw er;
  }
};


const createTruck = async ({type, userId}) => {
  const currentTypeInfo = truckInfo[type];
  if (!currentTypeInfo) {
    throw new Error('Invalid truck type');
  }

  const truck = await new Truck({
    created_by: new ObjectId(userId),
    type,
    ...currentTypeInfo,
  });

  await truck.save();
};

const asignTruckById = async ({truckId, userId}) => {
  const assignedTrucks = await Truck.find({assigned_to: userId});
  if (assignedTrucks.length) {
    const er = new Error('Driver already has assigned truck');
    er.status = 400;
    throw er;
  }
  const truck = await Truck.findOne({_id: truckId, created_by: userId});
  if (!truck) {
    const er = new Error('Truck not found');
    er.status = 400;
    throw er;
  }
  truck.assigned_to = new ObjectId(userId);
  await truck.save();
};

const findAvailableTruck = async ({payload, width, length, height}) => {
  return await Truck.findOne({
    'status': IS,
    'assigned_to': {$ne: null},
    'payload': {$gt: payload},
    'dimensions.width': {
      $gt: width,
    },
    'dimensions.length': {
      $gt: length,
    },
    'dimensions.height': {
      $gt: height,
    },
  });
};

module.exports = {
  createTruck,
  asignTruckById,
  getTrucksByUserId,
  getTruckByIds,
  updateTruckTypeByIds,
  updateTruckTypeById,
  deleteTruckById,
  getAssignedTruckIdByUserId,
  getAssignedTruckByUserId,
  findAvailableTruck,
  getAssignedTruckByTruckId,
};
